package exercise;

// BEGIN

import java.util.Arrays;
import java.util.stream.Stream;

public class App {

    public static String[][] enlargeArrayImage(String[][] image) {
        return Arrays.stream(image)
                .flatMap(row -> {
                    String[] enlargedRow = Arrays.stream(row)
                            .flatMap(elem -> Stream.of(elem, elem))
                            .toArray(String[]::new);
                    return Stream.of(enlargedRow, enlargedRow);
                }).toArray(String[][]::new);
    }
}
// END
