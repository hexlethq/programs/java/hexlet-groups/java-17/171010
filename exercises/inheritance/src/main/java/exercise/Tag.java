package exercise;

import java.util.Map;
import java.util.StringJoiner;

// BEGIN
public class Tag {

    protected String name;
    protected Map<String, String> attributes;

    protected Tag(String name, Map<String, String> attributes) {
        this.name = name;
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(" ", " ", "");
        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            String s = entry.getKey() + "=\"" + entry.getValue() + '"';
            joiner.add(s);
        }
        String attStr = attributes.isEmpty() ? "" : joiner.toString();
        return "<" + name + attStr + ">";
    }
}
// END
