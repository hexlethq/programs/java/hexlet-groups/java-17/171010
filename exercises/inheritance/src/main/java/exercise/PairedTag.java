package exercise;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

// BEGIN
public class PairedTag extends Tag {

    protected final String innerText;
    protected final List<Tag> children;

    protected PairedTag(String name, Map<String, String> attributes, String innerText, List<Tag> children) {
        super(name, attributes);
        this.innerText = innerText;
        this.children = children;
    }

    @Override
    public String toString() {
        String childrenStr = children.stream()
                .map(Object::toString)
                .collect(Collectors.joining());
        return super.toString() + innerText + childrenStr + "</" + name + ">";
    }
}
// END
