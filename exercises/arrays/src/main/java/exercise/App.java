package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {
        if (arr.length == 0) {
            return arr;
        }
        for (int iFromStart = 0, iFromEnd = arr.length - 1, tmp; iFromStart < iFromEnd; iFromStart++, iFromEnd--) {
            tmp = arr[iFromEnd];
            arr[iFromEnd] = arr[iFromStart];
            arr[iFromStart] = tmp;
        }
        return arr;
    }

    public static int getIndexOfMaxNegative(int[] arr) {
        int wrongResult = -1;
        int result = wrongResult;
        if (arr.length == 0) {
            return result;
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= 0) {
                continue;
            }
            if (result == wrongResult || arr[i] > arr[result]) {
                result = i;
            }
        }
        return result;
    }
    // END
}
