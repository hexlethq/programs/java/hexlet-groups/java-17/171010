package exercise;

import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class App {

    public static List<String> buildAppartmentsList(List<Home> apartments, int limit) {
        return apartments.stream()
                .sorted(Home::compareTo)
                .limit(limit)
                .map(Object::toString)
                .collect(Collectors.toList());
    }
}
// END
