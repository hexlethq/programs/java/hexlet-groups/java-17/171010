package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

// BEGIN
public class Validator {

    public static List<String> validate(Address address) {
        try {
            Field[] fields = address.getClass().getDeclaredFields();
            List<String> result = new ArrayList<>();
            for (Field field : fields) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(NotNull.class) && field.get(address) == null) {
                    result.add(field.getName());
                }
            }
            return result;
        } catch (IllegalAccessException exception) {
            exception.printStackTrace();
        }
        return null;
    }
}
// END
