package exercise;

class Converter {
    // BEGIN
    public static int convert(int x, String str) {
        if ("b".equals(str)) {
            return x * 1024;
        } else if ("kb".equals(str)) {
            return x / 1024;
        }
        return 0;
    }

    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }
    // END
}
