package exercise;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

// BEGIN

public class App {
    public static LinkedHashMap<String, String> genDiff(Map<String, Object> data1, Map<String, Object> data2) {
        TreeMap<String, String> result = new TreeMap<>(Comparator.naturalOrder());

        detectPresent(data1, data2, result);
        detectDeleted(data1, data2, result);
        detectAdded(data1, data2, result);
        LinkedHashMap<String, String> resultSorted = new LinkedHashMap<>();
        result.forEach(resultSorted::put);
        return resultSorted;
    }

    private static void detectPresent(Map<String, Object> data1, Map<String, Object> data2, TreeMap<String, String> result) {
        for (Map.Entry<String, Object> entry : data1.entrySet()) {
            data2.computeIfPresent(entry.getKey(), (key, value) -> {
                result.put(key, entry.getValue().equals(value) ? "unchanged" : "changed");
                return value;
            });
        }
    }

    private static void detectAdded(Map<String, Object> data1, Map<String, Object> data2, TreeMap<String, String> result) {
        data2.forEach((key, value) -> {
            if (!data1.containsKey(key)) {
                result.put(key, "added");
            }
        });
    }

    private static void detectDeleted(Map<String, Object> data1, Map<String, Object> data2, TreeMap<String, String> result) {
        data1.forEach((key, value) -> {
            if (!data2.containsKey((key))) {
                result.put(key, "deleted");
            }
        });
    }
}

//END


        