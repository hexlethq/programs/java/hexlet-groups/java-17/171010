package exercise;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List<Integer> numbers = List.of(1, 5, 8, 10, 4, 15);
        int sizeOfTake = 3;
        List<Integer> actual = App.take(numbers, sizeOfTake);
        assertThat(actual.size()).isEqualTo(sizeOfTake);
        // END
    }

    @Test
    void testTakeOutOfSizeMax() {
        // BEGIN
        List<Integer> numbers = List.of(1, 5, 8, 10, 4, 15);
        int sizeOfTake = 8;
        List<Integer> actual = App.take(numbers, sizeOfTake);
        assertThat(actual.size()).isEqualTo(numbers.size());
        // END
    }

    @Test
    void testTakeEmptyList() {
        // BEGIN
        List<Integer> numbers = Collections.emptyList();
        int sizeOfTake = 8;
        List<Integer> actual = App.take(numbers, sizeOfTake);
        assertThat(actual.size()).isZero();
        // END
    }
}
