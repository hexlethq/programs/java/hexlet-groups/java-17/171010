package exercise;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// BEGIN
public class App {

    public static final String ENVIRONMENT = "environment=\"";
    public static final String PREFIX = "X_FORWARDED_";

    public static String getForwardedVariables(String content) {
        List<String> envLines = getEnvironmentLines(content);
        List<String> variablesList = getEnvVariablesList(envLines);
        return variablesList.stream()
                .filter(elem -> elem.startsWith(PREFIX))
                .map(elem -> elem.substring(PREFIX.length()))
                .collect(Collectors.joining(","));
    }

    private static List<String> getEnvironmentLines(String content) {
        return Arrays.stream(content.split("\n"))
                .filter(line -> line.startsWith(ENVIRONMENT))
                .collect(Collectors.toList());
    }

    private static List<String> getEnvVariablesList(List<String> envLines) {
        return envLines.stream()
                .map(line -> line.substring(ENVIRONMENT.length(), line.length() - 1))
                .flatMap(line -> Stream.of(line.split(",")))
                .collect(Collectors.toList());
    }
}
//END
