package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {

    public static HashMap<String, Integer> getWordCount(String sentence) {
        String[] sentenceToArr = sentence.split(" ");
        HashMap<String, Integer> result = new HashMap<>();
        for (String word : sentenceToArr) {
            if (word.isBlank()) {
                continue;
            }
            result.computeIfPresent(word, (key, count) -> count + 1);
            result.putIfAbsent(word, 1);
        }
        return result;
    }

    public static String toString(HashMap<String, Integer> wordsCount) {
        if (wordsCount.isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        for (Map.Entry<String, Integer> entry : wordsCount.entrySet()) {
            sb.append("  ")
                    .append(entry.getKey())
                    .append(": ")
                    .append(entry.getValue())
                    .append('\n');
        }
        sb.append('}');
        return sb.toString();
    }
}
//END
