// BEGIN

package exercise;

import exercise.geometry.*;

public class App {
    public static double[] getMidpointOfSegment(double[][] segment) {
        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);

        double xBeginPoint = Point.getX(beginPoint);
        double yBeginPoint = Point.getY(beginPoint);

        double xEndPoint = Point.getX(endPoint);
        double yEndPoint = Point.getY(endPoint);
        double xNewPoint = (xBeginPoint + xEndPoint) / 2;
        double yNewPoint = (yBeginPoint + yEndPoint) / 2;

        return Point.makePoint(xNewPoint, yNewPoint);
    }

    public static double[][] reverse(double[][] segment) {
        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);

        double xBeginPoint = Point.getX(beginPoint);
        double yBeginPoint = Point.getY(beginPoint);
        double[] newEndPoint = Point.makePoint(xBeginPoint, yBeginPoint);

        double xEndPoint = Point.getX(endPoint);
        double yEndPoint = Point.getY(endPoint);
        double[] newBeginPoint = Point.makePoint(xEndPoint, yEndPoint);

        return Segment.makeSegment(newBeginPoint, newEndPoint);
    }
}
// END
