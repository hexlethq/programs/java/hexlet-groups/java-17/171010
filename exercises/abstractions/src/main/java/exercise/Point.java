package exercise;

class Point {
    // BEGIN

    public static int[] makePoint(int x, int y) {
        return new int[]{x, y};
    }

    public static int getX(int[] point) {
        return point[0];
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static String pointToString(int[] point) {
        return "(" + point[0] + ", " + point[1] + ")";
    }

    public static int getQuadrant(int[] point) {
        if (point[0] > 0 && point[1] > 0) {
            return 1;
        }
        if (point[0] > 0 && point[1] < 0) {
            return 4;
        }
        if (point[0] < 0 && point[1] > 0) {
            return 2;
        }
        if (point[0] < 0 && point[1] < 0) {
            return 3;
        }
        return 0;
    }
    // END
}
