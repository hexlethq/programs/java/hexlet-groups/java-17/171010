package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        boolean isExclamation = sentence.endsWith("!");
        if (isExclamation) {
            System.out.println(sentence.toUpperCase());
        } else {
            System.out.println(sentence.toLowerCase());
        }
        // END
    }
}
