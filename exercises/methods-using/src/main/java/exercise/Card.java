package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String str = cardNumber.substring(12);
        String star = "*";
        String result = star.repeat(starsCount) + str;
        System.out.println(result);
        // END
    }
}
