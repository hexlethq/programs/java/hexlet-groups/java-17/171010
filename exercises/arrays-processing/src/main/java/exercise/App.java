package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] getElementsLessAverage(int[] arr) {
        if (arr.length == 0) {
            return arr;
        }
        int sum = 0;
        int average;
        for (int a : arr) {
            sum += a;
        }
        average = sum / arr.length;
        int[] newArr = new int[arr.length];
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= average) {
                newArr[count] = arr[i];
                count++;
            }
        }
        int[] arrResult = new int[count];
        System.arraycopy(newArr, 0, arrResult, 0, count);
        return arrResult;
    }

    public static int getSumBeforeMinAndMax(int[] arr) {
        int min = Integer.MAX_VALUE;
        int minIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
                minIndex = i;
            }
        }
        int max = Integer.MIN_VALUE;
        int maxIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                maxIndex = i;
            }
        }
        int[] arrSlice = Arrays.copyOfRange(arr, Math.min(minIndex, maxIndex) + 1, Math.max(minIndex, maxIndex));
        int sum = 0;
        for (int a : arrSlice) {
            sum = sum + a;
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] numbers = {5, 4, 34, 8, 11, -5, 1};
        System.out.println(App.getSumBeforeMinAndMax(numbers));
    }
}
// END

