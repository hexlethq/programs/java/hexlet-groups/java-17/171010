package exercise;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AppTest {
    @Test
    void testGetElements1() {
        int[] numbers1 = {};
        int[] result1 = App.getElementsLessAverage(numbers1);
        assertThat(result1).isEmpty();

        int[] numbers2 = {0, 1, 2, 3, 4, 5, 10, 12};
        int[] result2 = App.getElementsLessAverage(numbers2);
        int[] expected2 = {0, 1, 2, 3, 4};
        assertThat(result2).containsExactly(expected2);
    }

    // BEGIN
    @Test
    void testGetSumBeforeMinAndMax() {
        int[] source = {5, 4, 34, 8, 11, -5, 1};
        int result = App.getSumBeforeMinAndMax(source);
        int expected = 19;
        assertThat(result).isEqualTo(expected);
    }

    @Test
    void testGetSumBeforeMinAndMax2() {
        int[] source = {7, 1, 37, -5, 11, 8, 1};
        int result = App.getSumBeforeMinAndMax(source);
        int expected = 0;
        assertThat(result).isEqualTo(expected);
    }
    // END
}
