package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        String[] wordsArray = str.split(" ");
        String result = "";
        for (String abbr : wordsArray) {
            result = result + abbr.charAt(0);

        }

        return result.toUpperCase();
    }


    // END
}
