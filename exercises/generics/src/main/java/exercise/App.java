package exercise;

// BEGIN

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class App {

    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> where) {
        List<Map<String, String>> result = new ArrayList<>(books);
        for (Map.Entry<String, String> entry : where.entrySet()) {
            for (Map<String, String> book : books) {
                if (!book.containsKey(entry.getKey()) || !book.get(entry.getKey()).equals(entry.getValue())) {
                    result.remove(book);
                }
            }
        }
        return result;
    }
}

//END
