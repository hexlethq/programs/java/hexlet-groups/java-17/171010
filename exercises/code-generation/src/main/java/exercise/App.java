package exercise;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.nio.file.Files;
import java.nio.file.Path;

// BEGIN
public class App {

    @SneakyThrows
    public static void save(Path path, Car car) {
        String serialized = car.serialize();
        Files.writeString(path, serialized);
    }

    @SneakyThrows
    public static Car extract(Path path) {
        return new ObjectMapper().readValue(path.toFile(), Car.class);
    }
}
// END
