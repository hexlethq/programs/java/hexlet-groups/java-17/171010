package exercise;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;

// BEGIN
@Getter
@AllArgsConstructor
// END
class Car {
    int id;
    String brand;
    String model;
    String color;
    User owner;

    @SneakyThrows
    public static Car unserialize(String jsonRepresentation) {
        return new ObjectMapper().readValue(jsonRepresentation, Car.class);
    }

    // BEGIN
    @SneakyThrows
    public String serialize() {
        return new ObjectMapper().writeValueAsString(this);
    }
    // END
}
