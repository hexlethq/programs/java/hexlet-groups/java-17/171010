package exercise.demo;

import lombok.Builder;
import lombok.NonNull;

@Builder
public class Item {
    @NonNull
    private final Integer id;
    private final String name;
    private final Double price;
}
