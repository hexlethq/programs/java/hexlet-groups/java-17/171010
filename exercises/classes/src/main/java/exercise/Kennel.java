package exercise;


// BEGIN

import java.util.Arrays;

public class Kennel {
    public static final int INIT_SIZE = 10;
    private static String[][] puppies = new String[INIT_SIZE][];
    private static int size = 0;

    public static void addPuppy(String[] puppy) {
        if (size >= puppies.length) {
            increaseArr();
        }
        puppies[size++] = puppy;
    }

    private static void increaseArr() {
        puppies = Arrays.copyOf(Kennel.puppies, Kennel.puppies.length * 2);
    }

    public static void addSomePuppies(String[][] puppies) {
        for (String[] puppy : puppies) {
            addPuppy(puppy);
        }
    }

    public static int getPuppyCount() {
        return size;
    }

    public static boolean isContainPuppy(String puppyName) {
        for (int i = 0; i < size; i++) {
            if (puppies[i][0].equals(puppyName)) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        String[][] result = new String[size][];
        System.arraycopy(puppies, 0, result, 0, size);
        return result;
    }

    public static String[] getNamesByBreed(String breed) {
        int nameSize = 0;
        for (int i = 0; i < size; i++) {
            if (puppies[i][1].equals(breed)) {
                nameSize++;
            }
        }
        String[] result = new String[nameSize];
        for (int i = 0, j = 0; i < size; i++) {
            if (puppies[i][1].equals(breed)) {
                result[j++] = puppies[i][0];
            }
        }
        return result;
    }

    public static void resetKennel() {
        size = 0;
        puppies = new String[INIT_SIZE][];
    }
}
// END
