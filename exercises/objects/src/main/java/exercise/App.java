package exercise;

import java.time.LocalDate;

class App {
    // BEGIN
    public static String buildList(String[] arr) {
        if (arr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<ul>\n");
        for (String str : arr) {
            sb.append("  <li>").append(str).append("</li>\n");
        }
        sb.append("</ul>");
        return sb.toString();
    }

    public static String getUsersByYear(String[][] users, int year) {
        StringBuilder sb = new StringBuilder();
        sb.append("<ul>\n");
        for (String[] str : users) {
            if (LocalDate.parse(str[1]).getYear() == year) {
                sb.append("  <li>").append(str[0]).append("</li>\n");
            }
        }
        sb.append("</ul>");
        if (sb.length() == 10) {
            return "";
        }
        return sb.toString();
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        return "";
        // END
    }
}
