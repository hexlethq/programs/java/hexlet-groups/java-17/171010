package exercise;

import java.util.ArrayList;
import java.util.List;

// BEGIN
public class App {
    public static boolean scrabble(String letters, String word) {
        if (letters.isEmpty()) {
            return false;
        }
        List<Character> listLetters = new ArrayList<>();
        for (int i = 0; i < letters.length(); i++) {
            Character charToAdd = Character.toLowerCase(letters.charAt(i));
            listLetters.add(charToAdd);
        }
        for (int i = 0; i < word.length(); i++) {
            Character charToRemove = Character.toLowerCase(word.charAt(i));
            boolean charRemoved = listLetters.remove(charToRemove);
            if (!charRemoved) {
                return false;
            }
        }
        return true;
    }
}
//END
