package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        if ((a + b) > c && (b + c) > a && (a + c) > b) {
            String scaleneTriangle = "Разносторонний";
            String isoscelesTriangle = "Равнобедренный";
            String equilateralTriangle = "Равносторонний";
            if (a != b && b != c && c != a) {
                return scaleneTriangle;
            }
            if (a == b && c == a) {
                return equilateralTriangle;
            }
            if (a == b || b == c || c == a) {
                return isoscelesTriangle;
            }
        }
        return "Треугольник не существует";
    }

    // END
}
